//
//  ViewController.swift
//  Tarea-01
//
//  Created by Aldair Raul Cosetito Coral on 7/6/20.
//  Copyright © 2020 Aldair Cosetito. All rights reserved.
//

import UIKit
import GoogleMaps

class ViewController: UIViewController {

    //MARK: - Properties
    var infoWindow: ViewInformation!
    var informacionMarker: [String: String] = [:]
    var mapView: GMSMapView!
    var locationMarker: GMSMarker?
    
    @IBOutlet weak var nombreTextField: TextFielCustomized!
    @IBOutlet weak var referenciaTextField: TextFielCustomized!
    @IBOutlet weak var direccionTextField: TextFielCustomized!
    @IBOutlet weak var contenidoLabel: UILabel!
    @IBOutlet weak var mapContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setUpMapView()
    }
    
    //MARK: - Init Mapview
    func setUpMapView(){
        mapView = GMSMapView()
        
        //Configuration
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.settings.zoomGestures = true
        mapView.settings.myLocationButton = true
        mapView.settings.allowScrollGesturesDuringRotateOrZoom = true
        mapView.settings.rotateGestures = true
        mapView.settings.scrollGestures = true
        
        mapView.translatesAutoresizingMaskIntoConstraints = false
        mapContainer.addSubview(mapView)
        
        NSLayoutConstraint.activate([
            mapView.topAnchor.constraint(equalTo: mapContainer.topAnchor, constant: 0),
            mapView.leadingAnchor.constraint(equalTo: mapContainer.leadingAnchor, constant: 0),
            mapView.bottomAnchor.constraint(equalTo: mapContainer.bottomAnchor, constant: 0),
            mapView.trailingAnchor.constraint(equalTo: mapContainer.trailingAnchor, constant: 0)
        ])
        
    }

    //MARK: - Agregar Action
    @IBAction func agregarButtonTapped(_ sender: Any) {
        
        if validarCamposDeIngreso() {
            self.informacionMarker["Nombre"] = "Nombre: \(nombreTextField.text ?? "")"
            self.informacionMarker["Direccion"] = "Direccion: \(direccionTextField.text ?? "")"
            self.informacionMarker["Referencia"] = "Referencia: \(referenciaTextField.text ?? "")"
            refrescarBottomLine()
        } else {
            mostrarAlerta(mensaje: "Debe completar los campos para poder agregar el marker.")
            refrescarBottomLine()
            return
        }
    }
    
    //MARK: - Metodos
    func validarCamposDeIngreso() -> Bool {
        
        self.informacionMarker = [:]
        
        nombreTextField.isValid = (nombreTextField.text?.count ?? 0 <= 0) ? false : true
        direccionTextField.isValid = (direccionTextField.text?.count ?? 0 <= 0) ? false : true
        referenciaTextField.isValid = (referenciaTextField.text?.count ?? 0 <= 0) ? false : true
        
        guard nombreTextField.isValid == true,
            direccionTextField.isValid == true,
            referenciaTextField.isValid == true else {
                
            return false
        }
        
        return true
    }
    
    func refrescarBottomLine() -> Void {
        nombreTextField.setColorBottomLine()
        direccionTextField.setColorBottomLine()
        referenciaTextField.setColorBottomLine()
    }
    
    func mostrarAlerta(mensaje: String) -> Void {
        let alert = UIAlertController(title: "Alerta", message: mensaje, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func reiniciarTextFields() -> Void {
        nombreTextField.text = ""
        referenciaTextField.text = ""
        direccionTextField.text = ""
    }
    
}


//MARK: - GMSMapViewDelegate
extension ViewController : GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        locationMarker?.map = nil
        
        if validarCamposDeIngreso() {
            self.informacionMarker["Nombre"] = "Nombre: \(nombreTextField.text ?? "")"
            self.informacionMarker["Direccion"] = "Direccion: \(direccionTextField.text ?? "")"
            self.informacionMarker["Referencia"] = "Referencia: \(referenciaTextField.text ?? "")"
            refrescarBottomLine()
            
            locationMarker?.map = nil
            locationMarker = GMSMarker(position: coordinate) //cordinateHome)
            locationMarker?.map = mapView
            
            reiniciarTextFields()
            
        } else {
            mostrarAlerta(mensaje: "Debe completar los campos para poder agregar el marker.")
            refrescarBottomLine()
            return
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        print(marker.position)
        return false
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        print("Marker: Latitude: \(String(marker.position.latitude)) - Longitude: \(String(marker.position.longitude))")
        infoWindow = ViewInformation()
        infoWindow.longitudeWindow.text = "Longitud: \(String(marker.position.longitude))"
        infoWindow.latitudeWindow.text = "Latitud: \(String(marker.position.latitude))"
        infoWindow.nombreLabel.text = self.informacionMarker["Nombre"]
        infoWindow.direccionLabel.text = self.informacionMarker["Direccion"]
        infoWindow.referenciaLabel.text = self.informacionMarker["Referencia"]
        return infoWindow
    }
    
    
}

