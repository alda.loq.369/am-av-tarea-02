//
//  ViewInformation.swift
//  Tarea-01
//
//  Created by Aldair Raul Cosetito Coral on 7/13/20.
//  Copyright © 2020 Aldair Cosetito. All rights reserved.
//

import UIKit

class ViewInformation: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    var latitudeWindow: UILabel!
    var longitudeWindow: UILabel!
    var nombreLabel: UILabel!
    var direccionLabel: UILabel!
    var referenciaLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    func setupView() -> Void {
        
        self.backgroundColor = UIColor.white
        self.layer.borderColor = UIColor.blue.cgColor
        self.layer.cornerRadius = CGFloat(20)
        self.frame = CGRect(x: 0, y: 0, width: 280, height: 400)
        
        longitudeWindow = UILabel()
        longitudeWindow.textColor = UIColor.black
        latitudeWindow = UILabel()
        latitudeWindow.textColor = UIColor.black
        
        nombreLabel = UILabel()
        nombreLabel.textColor = UIColor.black
        direccionLabel = UILabel()
        direccionLabel.textColor = UIColor.black
        referenciaLabel = UILabel()
        referenciaLabel.textColor = UIColor.black
        
        self.translatesAutoresizingMaskIntoConstraints = false
        nombreLabel.translatesAutoresizingMaskIntoConstraints = false
        direccionLabel.translatesAutoresizingMaskIntoConstraints = false
        referenciaLabel.translatesAutoresizingMaskIntoConstraints = false
        longitudeWindow.translatesAutoresizingMaskIntoConstraints = false
        latitudeWindow.translatesAutoresizingMaskIntoConstraints = false
        
        
        self.addSubview(nombreLabel)
        self.addSubview(direccionLabel)
        self.addSubview(referenciaLabel)
        self.addSubview(longitudeWindow)
        self.addSubview(latitudeWindow)
        
        NSLayoutConstraint.activate([
            self.widthAnchor.constraint(equalToConstant: 250),
            self.heightAnchor.constraint(equalToConstant: 100)
        ])
        
        NSLayoutConstraint.activate([
            //Nombre constraint
            nombreLabel.topAnchor.constraint(greaterThanOrEqualTo: self.topAnchor,
                                                 constant: 5),
            nombreLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor,
                                                     constant: 10),
            //Direccion constraint
            direccionLabel.topAnchor.constraint(greaterThanOrEqualTo: nombreLabel.topAnchor,
                                                 constant: 15),
            direccionLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor,
                                                     constant: 10),
            direccionLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor,
                                                      constant: 10),
            //Referencia constraint
            referenciaLabel.topAnchor.constraint(greaterThanOrEqualTo: direccionLabel.topAnchor,
                                                 constant: 15),
            referenciaLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor,
                                                     constant: 10),
            referenciaLabel.bottomAnchor.constraint(equalTo: direccionLabel.bottomAnchor,
                                                     constant: 15),
            referenciaLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor,
                                                      constant: 10),
            //Latitude constraint
            longitudeWindow.topAnchor.constraint(greaterThanOrEqualTo: referenciaLabel.topAnchor,
                                                 constant: 15),
            longitudeWindow.leadingAnchor.constraint(equalTo: self.leadingAnchor,
                                                     constant: 10),
            longitudeWindow.bottomAnchor.constraint(equalTo: referenciaLabel.bottomAnchor,
                                                     constant: 15),
            longitudeWindow.trailingAnchor.constraint(equalTo: self.trailingAnchor,
                                                      constant: 10),
            //Latitude constraint
            latitudeWindow.topAnchor.constraint(greaterThanOrEqualTo: longitudeWindow.topAnchor,
                                                 constant: 15),
            latitudeWindow.leadingAnchor.constraint(equalTo: self.leadingAnchor,
                                                     constant: 10),
            latitudeWindow.bottomAnchor.constraint(equalTo: longitudeWindow.bottomAnchor,
                                                     constant: 15),
            latitudeWindow.trailingAnchor.constraint(equalTo: self.trailingAnchor,
                                                      constant: 10)
        ])
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.masksToBounds = true
        layer.cornerRadius = 5
    }

}
