//
//  TextFielCustomized.swift
//  Tarea-01
//
//  Created by Aldair Raul Cosetito Coral on 7/7/20.
//  Copyright © 2020 Aldair Cosetito. All rights reserved.
//

import UIKit

@IBDesignable class TextFielCustomized: UITextField {

    // MARK: - Propiedades
    @IBInspectable var lineColor: UIColor = .black {
        didSet {
            bottomLineView.backgroundColor = lineColor
        }
    }
    
    @IBInspectable var isValid: Bool = true
    
    @IBInspectable var lineColorError: UIColor = .white{
        didSet{
            bottomLineView.backgroundColor = lineColorError
        }
    }
    
    var bottomLineView: UIView!

    // MARK: - Ciclo vida de un VIEW
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupTextField()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupTextField()
    }
    
    override func draw(_ rect: CGRect) {
        super.drawText(in: rect)
        setupTextField()
    }
    
    // MARK: - Metodos
    private func setupTextField() {
        borderStyle = .none
        bottomLineView = UIView()
        setColorBottomLine()
        bottomLineView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(bottomLineView)
        
        NSLayoutConstraint.activate([
            bottomLineView.heightAnchor.constraint(equalToConstant: 1),
            bottomLineView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
            bottomLineView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            bottomLineView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0)
        ])
    }
    
    //MARK: - Event for select propertie isValid
    func setColorBottomLine(){
        bottomLineView.backgroundColor = (isValid) ? lineColor : lineColorError
    }
}
